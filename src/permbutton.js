import React, { Component } from 'react';
import { askForPermissioToReceiveNotifications } from './push-notifications';

class NotificationButton extends Component {
    render() {
        return (

    <button className="notifications" onClick={askForPermissioToReceiveNotifications} >
      Turn On
    </button>
);
    }
}
export default NotificationButton;