import React, { Component } from 'react';
import '../Search/search.css';
import '../index.css';
import Gauge from 'react-svg-gauge';

class Block extends Component {

    render() {
        return (
            <div id="blockads">
                  <div class="card">
                    <div class="card-header">
                      <a class="card-link" data-toggle="collapse" href="#blockOne">
                      <i class="fas fa-caret-down mr-3" />
                        Winston Privacy
                        <div className="beshrating">
                          <Gauge className="beshrating" value={90} width={55} height={35} label='' color={'#b13d65'} />
                        </div>
                      </a>
                    </div>
                    <div id="blockOne" class="collapse" data-parent="#blockads">
                      <div class="card-body">
                        <p>Price: <strong>$249.99</strong></p>
                        <p><a class="btn btn-info" href='https://winstonprivacy.com/' role="button">Visit Site</a></p>
                        <p><strong>Compatibility:</strong> IoT Devices, websites, and Streaming services</p>
                      </div>
                    </div>
                  </div>
                </div>
        );
    }
}
export default Block;