import React, { Component } from 'react';
import './about.css';
import '../index.css';
import {aboutLogo} from '..';


class About extends Component {
    render() {
        return (
                <div id="aboutModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>About</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>
                                <img class="about_logo" src={aboutLogo} alt="logo" />
                                <h4>Beshadu 1.0 beta</h4>
                                <p>a free web vision</p>
                                <hr className="bline" />
                                <p className="btext">
                                    copyright 2019 Beshadu [Lovey Pale]<br/>
                                    All rights reserved.
                                </p>
                                <a href="http://beshadu.co.za/">
                                    visit website
                                </a>
                            </p>
                        </div>
                    </div>

                </div>
                </div>
                
            );

        }
}

export default About;