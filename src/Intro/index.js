import React, { Component } from 'react';
import './intro.css';
import { bake_cookie } from 'sfcookies';
import { intro_1, intro_2, intro_3 } from '..';

class Intro extends Component {
  constructor(props) {
    super(props);
    this._handleScroll = this._handleScroll.bind(this);
  }

  setCookie() {
    bake_cookie('beshadu', 'usertype');
    window.location.reload();
  }

  _handleScroll(ev){
    var elmnt = this.mainDiv.scrollLeft;
    var y = window.innerWidth;
    var x = (elmnt/y)*50;
    this.myBar.style.width = x + "%";
  }

    render() {
        return (
             <div ref={node =>this.mainDiv = node} className='login' onScroll=
             {this._handleScroll.bind(this)}>
               <div className="topNav">
               </div>
               <div>
               <div className="content">   
                  <img src={intro_2} className='intImage' alt='rocket' />
                  <h4>Everything you need</h4>
                  <p id="test"></p>
                  <p>To escape online tracking, censorship, ads<br/> and protect your privacy. </p>
                </div> 
               </div>
               <div className="slide_1">
               <div className="content">   
                  <img src={intro_1} className='intImage' alt='woman' />
                  <h4>Find the best privacy apps</h4>
                  <p>From search engines, ad-blockers to browsers,<br/><strong>ranked and tested by us - for you.</strong></p>

                </div> 
               </div>
               <div className="slide_2">
               <div className="content">   
                  <img src={intro_3} className='intImage' alt='rocket' />
                  <h4>Set yourself free</h4>
                  <p>Empower yourself with tips and<br/> guides to help you stay free. </p>
                  
                </div>  
              </div> 
               <div className="bottomNav">
               <div className="progress-container">
                  <div ref={node =>this.myBar = node}className="progress-bar" id="myBar"></div>
                </div>
                 <p>
                  <button className='proceed' onClick={this.setCookie}>Get Started</button>
                </p>
               </div>
             </div>
            );
 }
}
export default Intro;