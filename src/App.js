import React, { Component } from 'react';
import './index.css';
import Nav from './Nevigation/index';
import Menu from './Menu/index';
import About from './About/index';
import Intro from './Intro/index';
import { read_cookie } from 'sfcookies';

  class App extends Component {
    constructor(props){
      super(props);
      this.state ={ mode: undefined };
      this.state =  { userId: read_cookie('beshadu') }
    }
    UNSAFE_componentWillMount() {
      let whatToShow = this.state.userId;
      if(whatToShow !== 'usertype'){
        whatToShow = <Intro />;
      }else{
        whatToShow = <div className="container">
                        <Nav />
                        <Menu />
                        <About />  
                      </div>
      }
      this.setState({ mode: whatToShow});
    }

    render() {
      const userId = this.state;
      if (userId !== 'usertype'){
        return (
          <div>
            {this.state.mode}
          </div>
        );
      } 
  }
}

export default App;
