import React, { Component } from 'react';
import './browse.css';
import '../index.css';
import Gauge from 'react-svg-gauge';

class Browse extends Component {


    render() {
        return (
            <div id="browse_1">
                  <div class="card">
                    <div class="card-header">
                      <a class="card-link" data-toggle="collapse" href="#collapseOne">
                      <i class="fas fa-caret-down mr-3" />
                        Tor Browser
                        <div className="beshrating">
                          <Gauge className="beshrating" value={65} width={55} height={35} label='' color={'#e84b69'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseOne" class="collapse" data-parent="#browse_1">
                      <div class="card-body">
                      <p>User friendliness: <strong>8.5/10</strong></p>
                        <p>Landing page: <strong>5/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Is the landing page practical and useful or is it just a billboard for ads" /></p>
                        <p>Resource usage: <strong>6/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="How much system resources does it use - does it slow down a machine?" /></p>
                        <p><a class="btn btn-info" href='https://www.torproject.org/download/' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Windows, Mac, Linux, Android</p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                      <i class="fas fa-caret-down mr-3" />
                        Brave Browser
                        <div className="beshrating">
                          <Gauge className="beshrating" value={72} width={55} height={35} label='' color={'#af3c64'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseTwo" class="collapse" data-parent="#browse_1">
                    <div class="card-body">
                      <p>User friendliness: <strong>7/10</strong></p>
                        <p>Landing page: <strong>8.5/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Is the landing page practical and useful or is it just a billboard for ads" /></p>
                        <p>Resource usage: <strong>6/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="How much system resources does it use - does it slow down a machine?" /></p>
                        <p><a class="btn btn-info" href='https://play.google.com/store/apps/details?id=org.torproject.torbrowser' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Windows, Mac, Linux, Android, iOS</p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                      <i class="fas fa-caret-down mr-3" />
                        Epic Browser
                        <div className="beshrating">
                          <Gauge className="beshrating" value={53} width={55} height={35} label='' color={'#f3b464'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseThree" class="collapse" data-parent="#browse_1">
                    <div class="card-body">
                      <p>User friendliness: <strong>5/10</strong></p>
                        <p>Landing page: <strong>4/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Is the landing page practical and useful or is it just a billboard for ads" /></p>
                        <p>Resource usage: <strong>7/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="How much system resources does it use - does it slow down a machine?" /></p>
                        <p><a class="btn btn-info" href='https://play.google.com/store/apps/details?id=org.torproject.torbrowser' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Windows, Mac</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                      <i class="fas fa-caret-down mr-3" />
                        SRWare Iron
                        <div className="beshrating">
                          <Gauge className="beshrating" value={67} width={55} height={35} label='' color={'#ed4d6b'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseFour" class="collapse" data-parent="#browse_1">
                    <div class="card-body">
                      <p>User friendliness: <strong>8.5/10</strong></p>
                        <p>Landing page: <strong>3.5/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Is the landing page practical and useful or is it just a billboard for ads" /></p>
                        <p>Resource usage: <strong>8/10</strong><i className="fas fa-question-circle icon141" data-toggle="tooltip" title="How much system resources does it use - does it slow down a machine?" /></p>
                        <p><a class="btn btn-info" href='https://www.srware.net/iron/#downloads' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Windows, Mac, Linux, Android</p>
                      </div>
                    </div>
                  </div>
                </div>
        );
    }
}
export default Browse;