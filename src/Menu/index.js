import React, { Component } from 'react';
import './menu.scss';
import '../index.css';
import Search from '../Search/index';
import Browse from '../Browse/index';
import Block from '../Hardware/index';
import Vpn from '../VPN/index';
import { news1, news2, news3 } from '..';

class Menu extends Component {
    constructor(props) {
        super(props);
        //Set default states
        this.state = {
            search: 'search_up',
            browse: 'browse_up',
            vpn: 'vpn_up',
            block: 'block_up',
            menuhead: 'menuhead',
        };
      }
     
      //Handle button actions and update headings 
      contentSearch = () => {
          const currentView = this.state.search;
          this.setState({search: !currentView, browse: 'browse_down', vpn: 'vpn_down', block: 'vpn-down'});

      }
      contentBrowse = () => {
        const currentBrowse = this.state.browse;
        this.setState({browse: !currentBrowse, search: 'search_down', vpn: 'vpn_down', block: 'vpn-down'});
      }
      contentVPN = () => {
          const currentVpn = this.state.vpn;
          this.setState({
              vpn: !currentVpn, search: 'search-down', browse: 'browse_down', block: 'vpn-down'
          })
      }
      contentBlock = () => {
        const currentBlock = this.state.block;
        this.setState({
            block: !currentBlock, search: 'search-down', browse: 'browse_down', vpn: 'vpn-down'
        })
    }
    
  render() {
    return (
        <div className='headspace'>
           <div className='menubut_cont'> 
              <button className='menubut' onClick={this.contentSearch}>
                <i className="fas fa-search fa-1x" />
                <p>Search</p></button>
            <button className='menubut' onClick={this.contentBrowse}>
            <i class="fas fa-globe-europe fa-1x" />
            <p>Browse</p></button>
            <button className='menubut' onClick={this.contentBlock}>
                <i class="fas fa-hdd fa-1x" />
            <p>Hardware</p></button>
            <button className='menubut' onClick={this.contentVPN}>
            <i class="fas fa-user-shield fa-1x" />
            <p>VPN</p></button>
            </div>
            <div className='news'>
                <div className='nwHead'>
                    <h4>Be informed and stay safe</h4>
                    <p>Learn how to protect your privacy</p>
                </div>
                <div id="archives">
                  <button className="btn btn-outline-default button234">Archives</button>
                </div>
                <div className='tab_one N1'>
                    <div className="left">
                        <a className="new_345" href="https://www.thecreepyline.com">
                        <p><strong>THEY'RE TRACKING YOU</strong></p></a>
                        <p>You'll be shocked how much they know about you, and how that information is used.</p>
                    </div>
                    <div className="right">
                        <img className="newsImage" src={news2} alt="thief" />
                    </div>
                </div>
                <div className='tab_one N2'>
                   <div className='left'> 
                    <a className="new_345" href="https://youtu.be/dW7k_GZYLwk">
                    <p><strong>YOU ARE THE PRODUCT</strong></p></a>
                    <p>In this attension economy, you're the product.</p>
                  </div>
                  <div className="right">
                        <img className="newsImage" src={news1} alt="thief" />
                    </div>  
                </div>
                <div className='tab_one N3'>
                  <div className='left'>   
                    <a className="new_345" href="https://interactive.aljazeera.com/aje/2019/hail-algorithms/index.html">
                    <p><strong>ALL HAIL THE ALGORITHM</strong></p></a>
                    <p>Liking, retweeting and googling your privacy away. </p>
                  </div>
                  <div className="right">
                        <img className="newsImage" src={news3} alt="thief" />
                    </div> 
                </div>
            </div>
           <div className={this.state.search ? 'search_down': 'search_up'}>
               <div className='inner_content'>
               <h5 id='wnd'>Search</h5>
                 <h5>Search Privately</h5>
                 <p id='subtext'>They track your searches, even in private mode - but not these guys.</p>   
                <Search /> 
                </div> 
            </div>
            <div className={this.state.browse ? 'browse_down' : 'browse_up'}>
            <div className='inner_content'>
            <h5 id='wnd'>Browse</h5>
              <h5>Push back against online tracking!</h5>
              <p id='subtext'>Experience real private browsing without tracking, surveillance, or censorship.</p>
              <Browse />
            </div>
            </div>
            <div className={this.state.block ? 'vpn_down' : 'vpn_up'}>
            <div className='inner_content'>
            <h5 id='wnd'>Hardware</h5>   
            <h5>Plug & Play privacy Gadgets </h5>
              <p id='subtext'>Hardware designed solely for privacy.</p> 
              <Block />
            </div>
            </div>
             <div className={this.state.vpn ? 'vpn_down' : 'vpn_up'}>
            <div className='inner_content'>
            <h5 id='wnd'>VPN</h5>   
            <h5>Browse privately</h5>
              <p id='subtext'> and stop leaking your info </p> 
              <Vpn />
            </div>
            </div>   
        </div>
    );

        }
}

export default Menu;