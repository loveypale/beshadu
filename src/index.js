import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
export { default as slide1 } from './resources/slide1.png';
export { default as aboutLogo } from './resources/Icon.png';
export { default as  news1 } from './resources/consumer.png';
export { default as  news2 } from './resources/theif.png';
export { default as  news3 } from './resources/news3.png';
export { default as  vpn1 } from './resources/surfshark.svg';
export { default as  intro_1 } from './resources/intro_1.png';
export { default as  intro_2 } from './resources/intro_2.png';
export { default as  intro_3 } from './resources/intro_3.png';

ReactDOM.render(<App />, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()