import React, { Component } from 'react';
import './search.css';
import '../index.css';
import Gauge from 'react-svg-gauge';

class Search extends Component {

    render() {
        return (
            <div id="accordion">
                  <div class="card">
                    <div class="card-header">
                      <a class="card-link" data-toggle="collapse" href="#collapseOne">
                      <i class="fas fa-caret-down mr-3" />
                        DuckDuckGo 
                        <div className="beshrating">
                          <Gauge className="beshrating" value={80} width={55} height={35} label='' color={'#af3c64'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                      <div class="card-body">
                      <p>G Experience: <strong>8/10 </strong>
                      <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="A measure of how similar is the experience to that of Google" />
                      </p>
                        <p>Ads: <strong>YES </strong> 
                        <i className="fas fa-question-circle icon141" data-toggle="toorgb(238, 238, 238)ltip" title="bargb(238, 238, 238)sed on immediate search terms, can be disabled in the settings and aren't targeted at you" />
                        </p>
                        <p><i>Our privacy policy is simple: we don’t collect
or share any of your personal information</i>.</p>
                        <p><a class="btn btn-info" href='https://duckduckgo.com/' role="button">Launch</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                      <i class="fas fa-caret-down mr-3" />
                        Search Encrypt
                        <div className="beshrating">
                          <Gauge className="beshrating" value={60} width={55} height={35} label='' color={'#f24e6d'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                      <p>G Experience: <strong>6/10 </strong>
                      <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="A measure of how similar is the experience to that of Google" />
                      </p>
                        <p>Ads: <strong>YES </strong> 
                        <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="based on immediate search terms and aren't targeted at you" />
                        </p>
                        <p><i>Enhance your local browsing privacy! After 30 minutes of inactivity your search results expire and can no longer be viewed.</i></p>
                        <p><a class="btn btn-info" href='https://www.searchencrypt.com/?ptc=://mail' role="button">Launch</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                      <i class="fas fa-caret-down mr-3" />
                        Gibiru
                        <div className="beshrating">
                          <Gauge className="beshrating" value={75} width={55} height={35} label='' color={'#b13d65'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                      <p>G Experience: <strong>6.5/10 </strong>
                      <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="A measure of how similar is the experience to that of Google" />
                      </p>
                        <p>Ads: <strong>NO </strong> 
                        </p>
                        <p><i>We don’t log your searches, IP address or place cookies on your computer. This means there is not data to sell to advertisers or to use to retarget you..</i></p>
                        <p><a class="btn btn-info" href='https://gibiru.com/' role="button">Launch</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header">
                      <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                      <i class="fas fa-caret-down mr-3" />
                        Swisscows
                        <div className="beshrating">
                          <Gauge className="beshrating" value={55} width={55} height={35} label='' color={'#fbba67'} />
                        </div>
                      </a>
                    </div>
                    <div id="collapseFour" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                      <p>G Experience: <strong>5/10 </strong>
                      <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="AI powered modern and chile friendly experience" />
                      </p>
                        <p>Ads: <strong>NO </strong> 
                        </p>
                        <p><i>Since we NEVER collect your data, we NEVER track your data!  No IP address, no browser info, no details about location. Nothing.</i></p>
                        <p><a class="btn btn-info" href='https://swisscows.com/' role="button">Launch</a></p>
                      </div>
                    </div>
                  </div>

                  </div>
        );
    }
}
export default Search;