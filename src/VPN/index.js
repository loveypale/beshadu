import React, { Component } from 'react';
import './vpn.css';
import '../index.css';
import Gauge from 'react-svg-gauge';
import { vpn1 } from '..';

class Vpn extends Component {

    render() {
        return (
            <div id="vpn_accordion">
                  <div className="card">
                    <div className="card-header">
                      <a className="card-link" data-toggle="collapse" href="#VPNOne">
                      <i className="fas fa-caret-down mr-3" />
                            Windscribe 
                        <div className="beshrating">
                          <Gauge className="beshrating" value={60} width={55} height={35} label='' color={'#f24e6d'} />
                        </div>
                      </a>
                    </div>
                    <div id="VPNOne" className="collapse" data-parent="#vpn_accordion">
                      <div className="card-body">
                       <img src="https://cdn-images-1.medium.com/max/184/1*JeNsCju6IZsUlzfhkGvAuA@2x.png" className="windscribe" alt='Surfshark'/>
                      <p>Speed: <strong>9/10 <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="VPN Speed is subjective and depends heavily on your internet speed and other factors. - speedtest.net" /></strong></p>
                        <p>Streaming: <strong>YES <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Netflix, torrenting" /></strong></p>
                        <p>Logs: <strong>NO <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Does it keep connection logs, IP timestamps, session logs, or monitor your activity." /></strong></p>
                        <p>Price: <strong>$4.08/mo</strong></p>
                        <p><a className="btn btn-info" href='https://windscribe.com/' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Computer, Browser, Phone, TV, Router </p>
                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-header">
                      <a className="collapsed card-link" data-toggle="collapse" href="#VPNTwo">
                      <i className="fas fa-caret-down mr-3" />
                        CyberGhost
                        <div className="beshrating">
                          <Gauge className="beshrating" value={75} width={55} height={35} label='' color={'#af3c64'} />
                        </div>
                      </a>
                    </div>
                    <div id="VPNTwo" className="collapse" data-parent="#vpn_accordion">
                      <div className="card-body">
                      <p>  
                       <img src="https://www.cyberghostvpn.com/img/element/main-menu/cyberghost_menu_logo_dark@2x.png" className="cyberghost" alt='Cyberghost'/>
                       </p>
                      <p>Speed: <strong>8.5/10<i className="fas fa-question-circle icon141" data-toggle="tooltip" title="VPN Speed is subjective and depends heavily on your internet speed and other factors. - speedtest.net" /></strong></p>
                        <p>Streaming: <strong>YES<i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Netflix, torrenting and Kodi" /></strong></p>
                        <p>Logs: <strong>NO <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Does it keep connection logs, IP timestamps, session logs, or monitor your activity." /></strong></p>
                        <p>Price: <strong>$2.75/mo</strong></p>
                        <p><a className="btn btn-info" href='https://www.cyberghostvpn.com/en_US/' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Computer, Browser, Phone, TV </p>
                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-header">
                      <a className="collapsed card-link" data-toggle="collapse" href="#VPNThree">
                      <i className="fas fa-caret-down mr-3" />
                        Surfshark
                        <div className="beshrating">
                          <Gauge className="beshrating" value={90} width={55} height={35} label='' color={'#af3c64'} />
                        </div>
                      </a>
                    </div>
                    <div id="VPNThree" className="collapse" data-parent="#vpn_accordion">
                      <div className="card-body">
                      <p>  
                       <img src={vpn1} className="surfshark" alt='surfshark'/>
                       </p>
                       <p>Speed: <strong>9.5/10 <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="VPN Speed is subjective and depends heavily on your internet speed and other factors. - speedtest.net" /></strong></p>
                        <p>Streaming: <strong>Yes<i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Amazing results with Netflix" /></strong></p>
                        <p>Logs: <strong>NO <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Does it keep connection logs, IP timestamps, session logs, or monitor your activity." /></strong></p>
                        <p>Price: <strong>$1.99/mo</strong></p>
                        <p><a className="btn btn-info" href='https://surfshark.com/' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Computer, Browser, Phone, TV</p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header">
                      <a className="collapsed card-link" data-toggle="collapse" href="#VPNFour">
                      <i className="fas fa-caret-down mr-3" />
                      Private Internet Access
                        <div className="beshrating">
                          <Gauge className="beshrating" value={65} width={55} height={35} label='' color={'#f24e6d'} />
                        </div>
                      </a>
                    </div>
                    <div id="VPNFour" className="collapse" data-parent="#vpn_accordion">
                      <div className="card-body">
                        <p>  
                       <img src="https://www.privateinternetaccess.com/assets/PIALogo2x-84413b002e788098871d6ecd79b23cc242b1eeb9864fc65a42d3cce7915c287f.png" className="private" alt='private'/>
                       </p>
                      <p>Speed: <strong>7/10 <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="VPN Speed is subjective and depends heavily on your internet speed and other factors. - speedtest.net" /></strong></p>
                        <p>Streaming: <strong>YES <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Torrenting, NO Netflix though" /></strong></p>
                        <p>Logs: <strong>NO <i className="fas fa-question-circle icon141" data-toggle="tooltip" title="Does it keep connection logs, IP timestamps, session logs, or monitor your activity." /></strong></p>
                        <p>Price: <strong>$3.33/mo</strong></p>
                        <p><a className="btn btn-info" href='https://www.privateinternetaccess.com/' role="button">Download</a></p>
                        <p><strong>Compatibility:</strong> Computer, Browser, Phone</p>
                      </div>
                    </div>
                  </div>

                  </div>
        );
    }
}
export default Vpn;