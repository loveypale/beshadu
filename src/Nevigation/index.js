import React, { Component } from 'react';
import './head.css';
import '../index.css';

class Navigation extends Component {
  render() {
    return (
<nav className="navbar bg-dark navbar-dark">
  <a className="navbar-brand" href="/#">
    </a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <i class="fas fa-bars icon143" />
  </button>
  <div className="collapse navbar-collapse" id="collapsibleNavbar">
    <ul className="navbar-nav">
    <li className="nav-item">
        <a className="nav-link" href="mailto:beshadu@vivaldi.net" data-toggle="modal" data-target="#aboutModal">About Beshadu</a>
      </li> 
      <li className="nav-item">
        <a className="nav-link" href="mailto:beshadu@vivaldi.net">Contact us</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="https://twitter.com/Beshadu_">
        <i className="fab fa-twitter mr-2"></i>
        twitter
        </a>
      </li>   
    </ul>
  </div>
</nav>
);
  }
}

export default Navigation;