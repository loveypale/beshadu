import React, { Component } from 'react';
import './slider.css';
import '../index.css';
import {slide1, slide2, stepslide1, stepslide2, stepslide3} from '..';

class Carousel extends Component {
    render() {
        return (
            <div id="demo" className="carousel slide" data-ride="carousel">

  {/*The slideshow*/}
  <div className="carousel-inner">
    <div className="carousel-item active">
      <img className='carousel_img'src={slide1} alt="info-slide" />
    </div>
    <div className="carousel-item">
    </div>
    <div className="carousel-item">
      <img className='carousel_img' src={stepslide1} alt="info-slide"  />
    </div>
    <div className="carousel-item">
      <img className='carousel_img' src={stepslide2} alt="info-slide"  />
    </div>
    <div className="carousel-item">
      <img className='carousel_img' src={stepslide3} alt="info-slide"  />
    </div>
  </div>
  
  {/*Left and right controls*/}
  <a className="carousel-control-prev" href="#demo" data-slide="prev">
    <span className="carousel-control-prev-icon"></span>
  </a>
  <a className="carousel-control-next" href="#demo" data-slide="next">
    <span className="carousel-control-next-icon"></span>
  </a>
</div>
    );
 }
}
export default Carousel;